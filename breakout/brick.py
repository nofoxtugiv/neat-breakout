import pygame


class Brick:
    WIDTH = 70
    HEIGHT = 20

    def __init__(self, x, y, busted=False):
        self.x = x
        self.y = y
        self.x_change = 0
        self.y_change = 1
        self.busted = busted

    def draw(self, win):
        if not self.busted:
            pygame.draw.rect(
                win, (255, 255, 255), (self.x, self.y, self.WIDTH, self.HEIGHT)
            )

    def update(self):
        self.y += self.y_change
        self.rect.y = int(self.y)

        if self.rect.y <= 0:
            self.y_change = 1
        elif self.rect.y >= 500:
            self.y_change = -1
