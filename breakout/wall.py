from .brick import Brick


class Wall:
    def __init__(self, rows, cols):
        self.rows = rows
        self.cols = cols

        self.wall = []
        y = 40

        for row in range(self.rows):
            x = 30
            for col in range(self.cols):
                brick = Brick(x, y, False)
                self.wall.append(brick)
                x += Brick.WIDTH + 5
            y += Brick.HEIGHT + 5

    def draw(self, win):
        for brick in self.wall:
            brick.draw(win)

    def detect_col(self, ball):
        """
        " TODO:
        - additional collision improvement
        - refactor this mess
        """
        for brick in self.wall:
            if not brick.busted:
                if (  # bottom edge test
                    ball.y_vel < 0
                    and ball.x + ball.RADIUS > brick.x
                    and ball.x - ball.RADIUS < brick.x + brick.WIDTH
                    and ball.y + ball.y_vel < brick.y + brick.HEIGHT
                    and ball.y + ball.RADIUS > brick.y
                ):
                    brick.busted = True
                    ball.y_vel *= -1
                    return True
                elif (  # top edge test
                    ball.y_vel > 0
                    and ball.x + ball.RADIUS > brick.x
                    and ball.x - ball.RADIUS < brick.x + brick.WIDTH
                    and ball.y + ball.y_vel > brick.y
                    and ball.y < brick.y + brick.HEIGHT
                ):
                    brick.busted = True
                    ball.y_vel *= -1
                    return True
                elif (  # left edge test
                    ball.x_vel > 0
                    and ball.x + ball.x_vel > brick.x
                    and ball.x - ball.RADIUS < brick.x + brick.WIDTH
                    and ball.y + ball.RADIUS > brick.y
                    and ball.y < brick.y + brick.HEIGHT
                ):
                    brick.busted = True
                    ball.x_vel *= -1
                    return True
                elif (  # right edge test
                    ball.x_vel < 0
                    and ball.x + ball.RADIUS > brick.x
                    and ball.x + ball.x_vel < brick.x + brick.WIDTH
                    and ball.y + ball.RADIUS > brick.y
                    and ball.y < brick.y + brick.HEIGHT
                ):
                    brick.busted = True
                    ball.x_vel *= -1
                    return True
