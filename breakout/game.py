from .ball import Ball
from .paddle import Paddle
from .wall import Wall
import pygame

pygame.init()


class GameInfo:
    """
    These will be the agent states sent to the NEAT algorithm.
    These values will be used to determine fitness of each genome.
    """

    def __init__(self, hits, score, lives):
        self.hits = hits
        self.score = score
        self.lives = lives


class Game:
    """
    To use, initialize an instance of this class and call the .loop() method.
    """

    SCORE_FONT = pygame.font.SysFont("courier", 50)

    WHITE = (255, 255, 255)
    BLACK = (0, 0, 0)

    def __init__(self, window, window_width, window_height):
        self.window_width = window_width
        self.window_height = window_height

        self.paddle = Paddle(
            self.window_width // 2 - Paddle.WIDTH // 2,
            window_height - Paddle.HEIGHT - 20,
        )
        self.ball = Ball(self.window_width // 2, window_height - Paddle.HEIGHT - 50)
        self.wall = Wall(8, 10)

        self.score, self.hits, self.lives = 0, 0, 3
        self.window = window

    def _draw_score(self):
        score_text = self.SCORE_FONT.render(f"{self.score}", 1, self.WHITE)
        self.window.blit(
            score_text,
            (
                self.window_width // 6 - score_text.get_width() // 2,
                self.window_height - score_text.get_height() - 20,
            ),
        )

    def _draw_hits(self):
        hits_text = self.SCORE_FONT.render(
            f"{self.hits + self.right_hits}", 1, self.RED
        )
        self.window.blit(
            hits_text, (self.window_width // 2 - hits_text.get_width() // 2, 10)
        )

    def _handle_collision(self):
        """
        " TODO:
        " - Brick collision
        """
        ball = self.ball
        paddle = self.paddle
        wall = self.wall

        if ball.x + ball.RADIUS >= self.window_width:
            ball.x_vel *= -1
        elif ball.x - ball.RADIUS <= 0:
            ball.x_vel *= -1
        elif ball.y - ball.RADIUS <= 0:
            ball.y_vel *= -1
        elif ball.y + ball.RADIUS >= self.window_height:
            ball.y_vel *= -1  # TODO: remove when life loss is implemented

        if ball.y_vel > 0:
            if ball.x >= paddle.x and ball.x <= paddle.x + Paddle.WIDTH:
                if ball.y + ball.RADIUS >= paddle.y:
                    ball.y_vel *= -1

                    middle_x = paddle.x + Paddle.WIDTH / 2
                    difference_in_x = middle_x - ball.x
                    reduction_factor = (Paddle.WIDTH / 2) / ball.MAX_VEL
                    ball.x_vel = difference_in_x / reduction_factor * -1
                    self.hits += 1

        if wall.detect_col(ball):
            self.score += 1

    def draw(self, draw_score=True, draw_hits=False):
        self.window.fill(self.BLACK)

        if draw_score:
            self._draw_score()

        if draw_hits:
            self._draw_hits()

        self.paddle.draw(self.window)
        self.ball.draw(self.window)
        self.wall.draw(self.window)

    def move_paddle(self, left=True, up=True):
        """
        Move the paddle.

        :returns: boolean indicating if paddle movement is valid.
                  Movement is invalid if it causes paddle to go
                  off the screen
        """
        if left and self.paddle.x - Paddle.VEL < 0:
            return False
        if not left and self.paddle.x + Paddle.WIDTH > self.window_width:
            return False
        self.paddle.move(left)

        return True

    def loop(self):
        """
        Executes a single game loop.

        :returns: GameInformation instance stating score
                  and hits of each paddle.
        """
        self.ball.move()
        self._handle_collision()
        game_info = GameInfo(self.hits, self.score, self.lives)

        return game_info

    def reset(self):
        """Resets the entire game."""
        self.ball.reset()
        self.paddle.reset()
        self.score = 0
        self.hits = 0
        self.lives = 3
