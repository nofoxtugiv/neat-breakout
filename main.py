import pygame
from breakout import Game


def play():
    width, height = 800, 600
    win = pygame.display.set_mode((width, height))
    pygame.display.set_caption("NEAT Breakout by @NoFoxTuGiv")

    game = Game(win, width, height)

    run = True
    clock = pygame.time.Clock()
    while run:
        clock.tick(60)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                break

        game.loop()

        keys = pygame.key.get_pressed()
        if keys[pygame.K_a]:
            game.move_paddle(left=True)
        elif keys[pygame.K_d]:
            game.move_paddle(left=False)
        elif keys[pygame.K_ESCAPE]:
            pygame.quit()

        game.draw()

        pygame.display.update()

    pygame.quit()


if __name__ == "__main__":
    play()
